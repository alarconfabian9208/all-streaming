from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:1234@localhost/mydb_flask'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)
ma = Marshmallow(app)

# 
# type_account
class  Type_account(db.Model):
  __tablename__ = 'type_account'
  id = db.Column(db.Integer, primary_key=True)
  name= db.Column(db.String(45), unique=True, nullable=False)
  
  def __init__(self, name):
    self.name = name

class Type_account_schema(ma.Schema):
  class Meta:
    fields = ('id', 'name')
    
typeAccountSchema = Type_account_schema()
typesAccountSchema = Type_account_schema(many=True)


# 
# account
class  Account(db.Model):
  __tablename__ = 'account'
  id = db.Column(db.Integer, primary_key=True)
  type_account_id = db.Column(db.Integer, db.ForeignKey('type_account.id'))
  code = db.Column(db.String, unique=True, nullable=False)
  is_day = db.Column(db.Boolean)
  type_account = db.relationship('Type_account', backref=db.backref('account', lazy=True))
  state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False)
  
  
  def __init__(self, type_account_id, state_id, code, is_day):
    self.type_account_id = type_account_id,
    self.state_id = state_id
    self.code = code
    self.is_day = is_day

class Account_schema(ma.Schema):
  class Meta:
    fields = ('id', 'type_account_id', 'state_id', 'code', 'is_day')
    
accountSchema = Account_schema()
accountsSchema = Account_schema(many=True)


# 
# state
class  State(db.Model):
  __tablename__ = 'state'
  id = db.Column(db.Integer, primary_key=True)
  name= db.Column(db.String, unique=True, nullable=False)
  accounts = db.relationship('Account', backref='state', lazy=True)
  
  def __init__(self, name):
    self.name = name

class State_schema(ma.Schema):
  class Meta:
    fields = ('id', 'name')
    
stateSchema = State_schema()
statesSchema = State_schema(many=True)

# 
# user
class  User(db.Model):
  __tablename__ = 'user'
  id = db.Column(db.Integer, primary_key=True)
  name= db.Column(db.String, unique=True)
  
  def __init__(self, name):
    self.name = name

class User_schema(ma.Schema):
  class Meta:
    fields = ('id', 'name')
    
userSchema = User_schema()
usersSchema = User_schema(many=True)



######### methods ###########

# 
# crear un tipo de cuenta
@app.route('/account-type/create', methods=['POST'])
def create_account_type():
  try: 
    name = request.json['name']
    
    new_account_type= Type_account(name)
    db.session.add(new_account_type)
    db.session.commit()
    
    return typeAccountSchema.jsonify(new_account_type)
  except Exception as e:
    db.session.rollback()
    return jsonify({
      'success': False,
      'message': str(e)
    })
    
# 
# obtener el listado de tipo de cuenta
@app.route('/account-type')
def get_accounts_type():
  all_accounts_type = Type_account.query.all()
  result = typesAccountSchema.dump(all_accounts_type)
  
  return jsonify(result)

# 
# crear un estado
@app.route('/state/create', methods=['POST'])
def create_state():
  name = request.json['name']
  
  new_state= State(name)
  db.session.add(new_state)
  db.session.commit()
  
  return stateSchema.jsonify(new_state)

# 
# obtener el listado de estados
@app.route('/state')
def get_states():
  all_states = State.query.all()
  result = statesSchema.dump(all_states)
  
  return jsonify(result)

# 
# obtener el listado de cuentas
@app.route('/account')
def get_accounts():
  all_accounts = db.session.query(Account, Type_account, State).join(Type_account).join(State).filter(Account.type_account_id == Type_account.id and Account.state_id == State.id).all()
  return jsonify({
    'accounts': [{
      'id': account.id,
      'name': type_account.name,
      'type_account_id': account.type_account_id,
      'code':account.code,
      'is_day':account.is_day,
      'state_id': account.state_id,
      'state': state.name
    }for account, type_account, state in all_accounts]
  })
  
# 
# crear cuenta
@app.route('/account/create', methods=['POST'])
def create_account():
  try:
    type_account_id = request.json['type_account_id']
    state_id = request.json['state_id']
    code = request.json['code']
    is_day = request.json['is_day']
    
    new_account= Account(type_account_id, state_id, code, is_day)
    db.session.add(new_account)
    db.session.commit()
    
    return accountSchema.jsonify(new_account)
  except Exception as e:
    db.session.rollback()
    return jsonify({
      'success': False,
      'message': str(e)
    })
# 
# filtrar las cuentas
@app.route('/account/filter', methods=['GET'])
def get_accounts_filter():
  type_account_id = request.args.get('type_account_id',0)
  state_id = request.args.get('state_id',0)
  
  query = db.session.query(Account).join(State).join(Type_account)
  
  if type_account_id and type_account_id != '0':
    query = query.filter(Type_account.id == type_account_id)
  
  if state_id and state_id != '0':
    query = query.filter(State.id == state_id) 
   
  all_accounts = query.all()  
  
  return jsonify({
    'accounts': [{
      'id': account.id,
      'name':account.type_account.name,
      'type_account_id': account.type_account_id,
      'code':account.code,
      'is_day':account.is_day,
      'state_id': account.state_id,
      'state': account.state.name
    }for account in all_accounts]
  })
  
  
# 
# cambiar estado a en uso
@app.route('/account/<int:id>', methods=['PUT'])
def change_account_state(id):
  try:
    data = request.get_json()
        
    account = Account.query.get(id) 
    print("==>> account: ", account)
    
    if not account:
      return {'message':"No se ha encontrado la cuenta solicitada",
            'status': 404
            }
    account.state_id = data.get('state', account.state_id)
    db.session.add(account) 
    db.session.commit()
    return {'message':"Estado actualizado correnctamenete",
            'status': 200
            }
  except Exception as e:
    db.session.rollback()
    return jsonify({
      'success': False,
      'message': str(e)
    })
    
if __name__ == '__main__':
  app.run(debug=True)