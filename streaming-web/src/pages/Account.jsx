import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Loader from './Loader'
import { Modal, Form, Badge } from "react-bootstrap"

function HomePage() {

  const [accountsType, setAccountsType] = useState([]);
  const [accountType, setAccountType] = useState({});
  const [accounts, setAccounts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState(false)
  const [message, setMessage] = useState('')
  const [code, setCode] = useState('')
  const [isDay, setIsDay] = useState(false)

  const handleShow = () => {
    setAccountType({})
    setCode('')
    setIsDay(false)
    setShowModal(true)
    setError(false)
  }
  const handleClose = () => setShowModal(false);

  const addTask = (newTask) => {
    setAccounts([...accounts, newTask]);
  }

  useEffect(() => {
    setIsLoading(true)
    axios.get('http://localhost:5000/account')
      .then(response => {
        setError(false)
        setAccounts(response.data.accounts)
        setIsLoading(false)
      })
      .catch(error => {
        setError(true)
        setMessage(error)
        setIsLoading(false)
      })


    axios.get('http://localhost:5000/account-type')
      .then(response => {
        setAccountsType(response.data)
        setIsLoading(false)
      })
      .catch(error => {
        setError(error)
        setIsLoading(false)
      })

  }, [accounts.length]);

  const handleCreateAccount = (evt) => {
    evt.preventDefault()
    if (accountType !== '' && code !== 0) {
      let data = {
        'type_account_id': accountType.id,
        'state_id': 1,
        'code': code,
        'is_day': isDay
      }
      axios.post("http://localhost:5000/account/create", data)
        .then((response) => {
          if (response.status === 200 && response.data && response.data.id) {
            console.log('ingresamos ');
            setShowModal(false)
            addTask(response.data)
          } else {
            setError(true)
            setMessage('El codigo de la cuenta debe ser unico')
          }
        })
        .catch((error) => {
          setError(true)
          setMessage(error)
        });
    }
  }

  const handleUpdateAccountState = (id, change_state) => {
    axios.put(`http://localhost:5000/account/${id}`, {
      'state': change_state
    })
      .then(response => {
        if (response.status === 200) {
          addTask(response)
        }
      })
      .catch(error => {
        console.error('Ocurrió un error:', error);
      });
  }

  return (
    <>
      <div className='container'>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active" aria-current="page">Cuenta</li>
          </ol>
        </nav>
        {isLoading ?
          <Loader></Loader>
          :
          <div className='tipeAccount'>
            <button className='btn btn-primary m-3' onClick={handleShow}>Crear cuenta</button>
            <Modal show={showModal} onHide={handleClose} backdrop='static' keyboard={false} size="lg">
              <Modal.Header closeButton>
                <Modal.Title>Crear cuenta</Modal.Title>
              </Modal.Header>
              <Form onSubmit={handleCreateAccount}>
                <Modal.Body>
                  <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Tipo de cuenta</Form.Label>
                    <Form.Control required as="select" value={accountType.id} onChange={(e) => setAccountType(accountsType.filter((item) => `${item.id}` === `${e.target.value}`)[0])}>
                      <option>Selecciona...</option>
                      {
                        accountsType.map((item, index) => {
                          return <option key={index} value={item.id}>{item.name}</option>
                        })
                      }
                    </Form.Control>
                  </Form.Group>
                  <Form.Group controlId='code'>
                    <Form.Label column>Codigo</Form.Label>
                    <Form.Control
                      name='code' required type='text' placeholder='Ingrese el codigo de la cuenta'
                      value={code} onChange={(e) => setCode(e.target.value)} autoComplete="off"
                    ></Form.Control>
                    {
                      error &&
                      <h6 className='color-message'>{message}</h6>
                    }
                  </Form.Group>
                  <Form.Group controlId='is_day'>
                    <Form.Label column>Formato</Form.Label>
                    {['radio'].map((type) => (
                      <div key={`inline-${type}`} className="mb-3">
                        <Form.Check
                          inline
                          label="Día"
                          name="group1"
                          required
                          type={type}
                          onChange={(e) => setIsDay(true)}
                          id={`inline-${type}-1`}
                        />
                        <Form.Check
                          inline
                          label="Hora"
                          name="group1"
                          required
                          type={type}
                          onChange={(e) => setIsDay(false)}
                          id={`inline-${type}-2`}
                        />
                      </div>
                    ))}
                  </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                  <button className="btn btn-secondary" onClick={handleClose}>
                    Cerrar
                  </button>

                  <button type='submit' className="btn btn-primary">
                    Crear
                  </button>
                </Modal.Footer>
              </Form>
            </Modal>
            <div className='card'>
              {accounts && accounts.length > 0 ?
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Tipo de cuenta</th>
                      <th scope="col">Código</th>
                      <th scope="col">Formato</th>
                      <th scope="col">Estado</th>
                      <th scope="col-3">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      accounts.map((element) => (
                        <tr key={element.id}>
                          <td>{element.name}</td>
                          <td>{element.code}</td>
                          <td>
                            {
                              element.is_day ?
                                <h6>Día</h6>
                                :
                                <h6>Hora</h6>
                            }
                          </td>
                          <td>{
                            element.state_id === 1 ?
                              <Badge bg="warning">Disponible</Badge>
                              :
                              element.state_id === 2 ?
                                <Badge bg="success">En uso</Badge>
                                :
                                <Badge bg="danger">Bloqueado</Badge>
                          }
                          </td>
                          <td className='col-3'>
                            <div className='row'>
                              {element.state_id === 1 &&
                                <>
                                  <div className='col'>
                                    <button type="button" className='btn btn-success' onClick={() => handleUpdateAccountState(element.id, 2)}>Utilizar</button>
                                  </div>
                                  <div className='col'>
                                    <button type="button" className="btn btn-danger" onClick={() => handleUpdateAccountState(element.id, 3)}>Bloquear</button>
                                  </div>
                                </>

                              }
                              {element.state_id === 2 &&
                                <>
                                  <div className='col'>
                                    <button type="button" className='btn btn-warning' onClick={() => handleUpdateAccountState(element.id, 1)}>Habilitar</button>
                                  </div>
                                  <div className='col'>
                                    <button type="button" className="btn btn-danger" onClick={() => handleUpdateAccountState(element.id, 3)}>Bloquear</button>
                                  </div>
                                </>
                              }
                              {element.state_id === 3 &&
                                <>
                                  <div className='col'>
                                    <button type="button" className='btn btn-success' onClick={() => handleUpdateAccountState(element.id, 2)}>Utilizar</button>
                                  </div>
                                  <div className='col'>
                                    <button type="button" className="btn btn-warning" onClick={() => handleUpdateAccountState(element.id, 1)}>Habilitar</button>
                                  </div>
                                </>
                              }
                            </div>
                          </td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
                :
                <h6>No se han encontrado datos para visualizar</h6>
              }

            </div>
          </div>
        }
      </div>
    </>
  )
}

export default HomePage