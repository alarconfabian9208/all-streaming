import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Loader from './Loader'
import { Modal, Form } from "react-bootstrap"

function TipeAccount() {

  const [accountsType, setAccountsType] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [name, setName] = useState('')
  const [error, setError] = useState(false)
  const [message, setMessage] = useState('')

  const handleShow = () => {
    setShowModal(true)
    setError(false)
    setName('')
  };
  const handleClose = () => setShowModal(false);

  useEffect(() => {
    setIsLoading(true)
    axios.get('http://localhost:5000/account-type')
      .then(response => {
        setAccountsType(response.data)
        setIsLoading(false)
      })
      .catch(error => {
        setError(true)
        setMessage(error)
      })
  }, [accountsType.length]);

  const addTask = (newTask) => {
    setAccountsType([...accountsType, newTask]);
  }

  const handleCreateAccountType = (event) => {
    event.preventDefault();
    if (name !== '') {
      let data = {
        'name': name
      }
      axios.post("http://localhost:5000/account-type/create",data)
        .then((response) => {
          console.log('response: ', response);
          if(response.data && response.data.id && response.status === 200){
            setError(false)
            setShowModal(false)
            addTask(response.data)
          }else{
            setError(true)
            setMessage('No se pueden ingresar un tipo de cuenta ya creado')
          }
        })
        .catch((error) => {
          setError(true)
          setMessage(error)
        });
    }
  }

  return (
    <>
      <div className='container'>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active" aria-current="page">Tipo cuenta</li>
          </ol>
        </nav>
        {isLoading ?
          <Loader></Loader>
          :
          <div className='tipeAccount'>
            <button className='btn btn-primary m-3' onClick={handleShow}>Crear tipo cuenta</button>
            <Modal show={showModal} onHide={handleClose} backdrop='static' keyboard={false} size="lg">
              <Modal.Header closeButton>
                <Modal.Title>Crear tipo de cuenta</Modal.Title>
              </Modal.Header>
              <Form onSubmit={handleCreateAccountType}>
                <Modal.Body>
                  <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Nombre</Form.Label>
                    <Form.Control
                      type="text"
                      onChange={(e) => setName(e.target.value)}
                      required
                      placeholder="ej. Netflix"
                      autoFocus
                    />
                  </Form.Group>
                  {
                    error &&
                      <h6 className='color-message'>{message}</h6>
                  }
                </Modal.Body>

                <Modal.Footer>
                  <button className="btn btn-secondary" onClick={handleClose}>
                    Cerrar
                  </button>

                  <button type='submit' className="btn btn-primary">
                    Crear
                  </button>
                </Modal.Footer>
              </Form>
            </Modal>
            <div className='card'>

              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">Nombre</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    accountsType.map((element) => (
                      <tr key={element.id}>
                        <td>{element.name}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        }
      </div>
    </>

  )
}

export default TipeAccount