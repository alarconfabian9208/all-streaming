import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Modal, Form, Badge } from "react-bootstrap"

function HomePage() {

  const [accountsType, setAccountsType] = useState([]);
  const [accountType, setAccountType] = useState({});
  const [accounts, setAccounts] = useState([]);
  const [states, setStates] = useState([]);
  const [state, setState] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [typeAccountId, setTypeAccountId] = useState('');
  const [stateId, setStateId] = useState('')
  const [error, setError] = useState('')

  useEffect(() => {
    setIsLoading(true)
    axios.get('http://localhost:5000/account-type')
      .then(response => {
        setAccountsType(response.data)
        setIsLoading(false)
      })
      .catch(error => {
        setError(error)
        setIsLoading(false)
      })

    axios.get('http://localhost:5000/state')
      .then(response => {
        setStates(response.data)
        setIsLoading(false)
      })
      .catch(error => {
        setError(error)
        setIsLoading(false)
      })


    axios.get('http://localhost:5000/account')
      .then(response => {
        setAccounts(response.data.accounts)
        setIsLoading(false)
      })
      .catch(error => {
        setError(error)
        setIsLoading(false)
      })
  }, []);


  const handleFilter = () => {
    if ((typeAccountId) || (stateId)) {
      axios.get(`http://localhost:5000/account/filter?type_account_id=${typeAccountId ? typeAccountId.id : 0}&state_id=${stateId ? stateId.id : 0}`)
        .then(response => {
          console.log('response: ', response.data.accounts);
          setAccounts(response.data.accounts)
          setIsLoading(false)
        })
        .catch(error => {
          setError(error)
          setIsLoading(false)
        })
    } else {
      axios.get('http://localhost:5000/account')
        .then(response => {
          setAccounts(response.data.accounts)
          setIsLoading(false)
        })
        .catch(error => {
          setError(error)
          setIsLoading(false)
        })
    }
  }


  return (
    <div className='homePage'>
      <div className='container'>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item active" aria-current="page">Inicio</li>
          </ol>
        </nav>

        <div className='row style-home'>
          <div className='col-3 text-position'>
            <Form.Group className="mb-3" controlId="account-type">
              <Form.Label>Tipo de cuenta</Form.Label>
              <Form.Control required as="select" value={accountType.id} onChange={(e) => setTypeAccountId(accountsType.filter((item) => `${item.id}` === `${e.target.value}`)[0])}>
                <option value={0}>Selecciona...</option>
                {
                  accountsType.map((item, index) => {
                    return <option key={index} value={item.id}>{item.name}</option>
                  })
                }
              </Form.Control>
            </Form.Group>
          </div>
          <div className='col-3 text-position'>
            <Form.Group className="mb-3" controlId="states">
              <Form.Label>Estado</Form.Label>
              <Form.Control required as="select" value={state.id} onChange={(e) => setStateId(states.filter((item) => `${item.id}` === `${e.target.value}`)[0])}>
                <option>Selecciona...</option>
                {
                  states.map((item, index) => {
                    return <option key={index} value={item.id}>{item.name}</option>
                  })
                }
              </Form.Control>
            </Form.Group>
          </div>
          <div className='col-2 style-button'>
            <button type="button" className="btn btn-primary" onClick={handleFilter}>Filtrar</button>
          </div>
        </div>

        <div className='card'>
          {accounts && accounts.length>0?
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Tipo de cuenta</th>
                  <th scope="col">Código</th>
                  <th scope="col">Formato</th>
                  <th scope="col">Estado</th>
                  <th scope="col-2">Acciones</th>
                </tr>
              </thead>
              <tbody>
                {
                  accounts.map((element) => (
                    <tr key={element.id}>
                      <td>{element.name}</td>
                      <td>{element.code}</td>
                      <td>
                        {
                          element.is_day ?
                            <h6>Día</h6>
                            :
                            <h6>Hora</h6>
                        }
                      </td>
                      <td>{
                        element.state_id === 1 ?
                          <Badge bg="warning">Disponible</Badge>
                          :
                          element.state_id === 2 ?
                            <Badge bg="success">En uso</Badge>
                            :
                            <Badge bg="danger">Bloqueado</Badge>
                      }
                      </td>
                      <td className='col-2'>
                        <div className='row'>
                          <div className='col'>
                            <button type="button" className='btn btn-primary' disabled={element.state_id !== 1} >Arrendar</button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
            :
            <h6>No se han encontrado datos para visualizar</h6>
          }

        </div>
      </div>
    </div>
  )
}

export default HomePage