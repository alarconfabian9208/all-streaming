import './App.css';
import { Route, Routes, Link } from 'react-router-dom'
import HomePage from './pages/HomePage';
import Account from './pages/Account';
import TipeAccount from './pages/TipeAccount';



function App() {
  return (
    <div className='App'>
      <nav className="navbar navbar-expand-lg bg-dark navbar-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">Streaming-app</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">Inicio</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/account">Cuenta</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/account-tipe">Tipo cuenta</Link>
              </li>
            </ul>
            <span className="navbar-text">
              <Link className="nav-link" to="#">Log out</Link>
            </span>
          </div>
        </div>
      </nav>
      <Routes>
        <Route path='/' element={<HomePage/>} />
        <Route path='/account' element={<Account/>} />
        <Route path='/account-tipe' element={<TipeAccount/>} />
      </Routes>
    </div>

  );
}

export default App;
